import Model.Field.Field;
import Model.Field.FieldFactory;

/**
 * Main class/method as entry point for compiler
 */
public class main
{
    public static void main(String args[])
    {
        FieldFactory fieldFactory = new FieldFactory();
        Field field = fieldFactory.createField(15, 15, 20);

        System.out.println(field.toString());
    }
}
