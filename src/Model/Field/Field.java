package Model.Field;

import Model.Tile.Tile;

/**
 * Model.Field model, contains multiple Tiles
 */
public class Field
{
    /**
     * Two-dimensional array of tiles
     */
    private Tile[][] tiles;

    /**
     * Model.Field model, contains multiple Tiles
     * @param tiles Two-dimensional array of tiles
     */
    public Field(Tile[][] tiles)
    {
        this.tiles = tiles;
    }

    /**
     * Get a tile from X and Y position of the field
     * @param x X-Coordinate, e.g. line
     * @param y Y-Coordinate, e.g. column
     * @return Model.Tile at given position
     */
    public Tile getTileAt(int x, int y)
    {
        return this.tiles[x][y];
    }

    /**
     * String representation of this field
     * @return String
     */
    public String toString()
    {
        String output = "";
        int a, b;
        for (a = 0; a < tiles.length; a++) {
            for (b = 0; b < tiles[0].length; b++) {
                Tile tile = this.getTileAt(a, b);

                int mineCount = tile.getMineCountInNeighbourhood();
                output += " " + (tile.hasMine() ? "@" : (mineCount > 0 ? mineCount : " ")) + " ";
            }

            output += "\n";
        }

        return output;
    }
}
