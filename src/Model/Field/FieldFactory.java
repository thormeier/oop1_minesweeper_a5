package Model.Field;

import Model.Tile.TileFactory;

public class FieldFactory
{
    /**
     * Model.Tile factory
     */
    private TileFactory tileFactory = new TileFactory();

    /**
     *
     * @param width         Width of the field
     * @param height        Height of the field
     * @param numberOfMines Number of randomly placed mines
     * @return A field
     */
    public Field createField(int width, int height, int numberOfMines)
    {
        return new Field(this.tileFactory.createTiles(width, height, numberOfMines));
    }
}
