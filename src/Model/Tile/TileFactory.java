package Model.Tile;

import java.util.Random;

/**
 * Factory for a two-dimensional array of Tiles
 */
public class TileFactory
{
    /**
     *
     * @param width         Width of the field
     * @param height        Height of the field
     * @param numberOfMines Number of lines
     * @return Two-dimensional array of fully setup Tiles
     */
    public Tile[][] createTiles(int width, int height, int numberOfMines)
    {
        Tile[][] rawField = new Tile[width][height];

        for (int x = 0; x < height; x++) {
            for (int y = 0; y < width; y++) {
                rawField[x][y] = new Tile(false);
            }
        }

        for (int x = 0; x < height; x++) {
            for (int y = 0; y < width; y++) {
                rawField = this.attachNeighboursToTileAt(x, y, rawField);
            }
        }

        rawField = this.attachMines(rawField, numberOfMines);

        return rawField;
    }

    /**
     * Attaches all neighbour Tiles of a field to a Model.Tile at a given position
     * @param x     X-Coordinate of Model.Tile that needs neighbours
     * @param y     Y-Coordinate of Model.Tile that needs neighbours
     * @param tiles All tiles
     * @return Altered array of Tiles
     */
    private Tile[][] attachNeighboursToTileAt(int x, int y, Tile[][] tiles)
    {
        int minX = 0;
        int maxX = tiles.length;

        int minY = 0;
        int maxY = tiles[0].length;

        // 8 is maximum possible neighbours
        for (int line = x - 1; line <= x + 1; line++) {
            for (int col = y - 1; col <= y + 1; col++) {
                if (
                    !(line == x && col == y)
                    && line >= minX
                    && line < maxX
                    && col >= minY
                    && col < maxY
                ) {
                    tiles[x][y].addNeighbour(tiles[line][col]);
                }
            }
        }

        return tiles;
    }

    /**
     * Sets a given number of mines at random places
     * @param tiles         Array of tiles
     * @param numberOfMines Number of mines to set randomly
     * @return Altered array of tiles
     */
    private Tile[][] attachMines(Tile[][] tiles, int numberOfMines)
    {
        int maxX = tiles.length;
        int maxY = tiles[0].length;

        Random r = new Random();

        while (numberOfMines > 0) {
            int x = r.nextInt(maxX);
            int y = r.nextInt(maxY);

            if (tiles[x][y].hasMine()) {
                continue;
            }

            numberOfMines--;
            tiles[x][y].setHasMine(true);
        }

        return tiles;
    }
}
