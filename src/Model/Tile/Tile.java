package Model.Tile;

import java.util.ArrayList;

/**
 * Model.Tile value object to be used within field
 */
public class Tile
{
    /**
     * Array of tiles, all neighbours of this tile
     */
    private ArrayList<Tile> neighbours;

    /**
     * Boolean flag to determine if the tile has a mine
     */
    private boolean hasMine;

    /**
     * Model.Tile value object to be used within field
     *
     * @param hasMine set if the tile has a mine
     */
    public Tile(boolean hasMine)
    {
        this.hasMine = hasMine;

        this.neighbours = new ArrayList<>();
    }

    /**
     * Add a new neighbour to this tile
     *
     * @param neighbour New neighbour of this tile
     */
    public void addNeighbour(Tile neighbour)
    {
        this.neighbours.add(neighbour);
    }

    /**
     * Determines the number of mines in this tile's direct neighbourhood
     *
     * @return number of mines of this tile's neighbours
     */
    public int getMineCountInNeighbourhood()
    {
        int numberOfMines = 0;

        for (Tile neighbour : this.neighbours) {
            if (neighbour.hasMine()) {
                numberOfMines++;
            }
        }

        return numberOfMines;
    }

    /**
     * Determines if this tile has a mine
     *
     * @return true if this field has a mine
     */
    public boolean hasMine()
    {
        return this.hasMine;
    }

    /**
     * Set the state of the mine of this tile
     * @param hasMine true if it has one
     */
    public void setHasMine(boolean hasMine)
    {
        this.hasMine = hasMine;
    }
}
